Ottivo / Employee's vacation days
=================================

This Command displays employee's yearly vacation days by a given year.

Requirement:

You will require php and [composer](https://getcomposer.org) to run this command

Install Dependencies with composer:
```shell script
$ composer install
```

To run the command:
```shell script
$ php bin/console employee-vacation-days 2021
```

To test with phpunit:
```shell script
$ ./vendor/bin/phpunit tests
```