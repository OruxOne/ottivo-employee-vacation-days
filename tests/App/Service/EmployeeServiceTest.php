<?php

declare(strict_types = 1);

namespace App\Service;

use Ottivo\App\Service\EmployeeService;
use PHPUnit\Framework\TestCase;

class EmployeeServiceTest extends TestCase
{
    public function testEmpty() {
        $sut = new EmployeeService();
        $this->assertEmpty($sut->getVacationDays(1990));
    }

    public function testYear2001() {
        $sut = new EmployeeService();
        $result = $sut->getVacationDays(2001);

        $this->assertCount(2, $result);
        $this->assertEquals(30, $result[0][1]);
        $this->assertEquals(26.5, $result[1][1]);
    }

    public function testYear2021() {
        $sut = new EmployeeService();
        $result = $sut->getVacationDays(2021);

        $this->assertCount(5, $result);
        $this->assertEquals(34, $result[0][1]);
        $this->assertEquals(31, $result[1][1]);
        $this->assertEquals(27, $result[2][1]);
        $this->assertEquals(30, $result[3][1]);
        $this->assertEquals(28, $result[4][1]);
    }
}