<?php

declare(strict_types=1);

namespace Ottivo\App\Command;

use Ottivo\App\Service\EmployeeService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class EmployeeCommand
 * @package Ottivo\App\Command
 */
class EmployeeCommand extends Command
{
    private const NAME = 'employee-vacation-days';

    protected function configure()
    {
        $this->setName(self::NAME)
            ->setDescription('Displays employee\'s yearly vacation days')
            ->setHelp(self::getHelpText())
            ->addArgument('year', InputArgument::REQUIRED, 'Pass the year.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $year = $input->getArgument('year');

        if (!is_numeric($year)) {
            $output->writeln('the year must be a number like: 2021');
            return Command::FAILURE;
        }

        $table = new Table($output);
        $table
            ->setHeaders([
                [new TableCell('Employee vacation days in year ' . $year, ['colspan' => 2])],
                ['Name', 'Vacation days'],
            ])
            ->setRows((new EmployeeService())->getVacationDays((int)$year));
        $table->render();

        return Command::SUCCESS;
    }

    private static function getHelpText(): string
    {
        return "Displays employee's yearly vacation days by a given year.\n"
            . 'Usage: ' . self::NAME . " \<year>\n"
            . 'Example: ' . self::NAME . ' 2021';
    }
}
