<?php

declare(strict_types=1);

namespace Ottivo\App\Service;

/**
 * Class EmployeeService
 * @package Ottivo\App\Service
 */
class EmployeeService
{
    /**
     * @param string $year
     * @return array|[[name, vacationDays]]
     * @throws \Exception
     */
    public function getVacationDays(int $year): array
    {
        $employeesData = [
            [
                'name' => 'Hans Müller',
                'dateOfBirth' => '30.12.1950',
                'ContractStartDate' => '01.01.2001',
            ], [
                'name' => 'Angelika Fringe',
                'dateOfBirth' => '09.06.1966',
                'ContractStartDate' => '15.01.2001',
            ], [
                'name' => 'Peter Klever',
                'dateOfBirth' => '12.07.1991',
                'ContractStartDate' => '15.05.2016',
                'specialContract' => 27,
            ], [
                'name' => 'Marina Helter',
                'dateOfBirth' => '26.01.1970',
                'ContractStartDate' => '15.01.2018',
            ], [
                'name' => 'Sepp Meier',
                'dateOfBirth' => '23.05.1980',
                'ContractStartDate' => '01.12.2017',
            ],
        ];

        $result = [];
        foreach ($employeesData as $employeeData) {
            $vacationDays = $this->calculateVacationDays($employeeData, $year);
            if ($vacationDays > 0) {
                $result [] = [$employeeData['name'], $vacationDays];
            }
        }

        return $result;
    }

    /**
     * @param array $employeeData
     * @param int $year
     * @return float
     * @throws \Exception
     */
    private function calculateVacationDays(array $employeeData, int $year): float
    {
        if (empty($employeeData['ContractStartDate'])) {
            return 0;
        }

        $contractStart = new \DateTime($employeeData['ContractStartDate']);
        $yearOfContractStart = (int)$contractStart->format('Y');

        if ($yearOfContractStart > $year) {
            return 0;
        }

        $vacationDays = $employeeData['specialContract'] ?? 26;

        $employeeAge = (new \DateTime('31.12.' . $year))->diff(new \DateTime($employeeData['dateOfBirth']));

        $ageForAdditionalVacation = $employeeAge->y - 30;

        if ($ageForAdditionalVacation > 4) {
            $vacationDays += floor($ageForAdditionalVacation / 5);
        }

        if ($yearOfContractStart !== $year || ((int)$contractStart->format('z') === 0)) {
            return $vacationDays;
        }

        $monthOfContractStart = (int)$contractStart->format('n');
        $dayOfContractStart = (int)$contractStart->format('j');

        $vacationDays -= (($monthOfContractStart - 1) * $vacationDays / 12) + ($dayOfContractStart === 1 ? 0 : 0.5);

        return round($vacationDays * 2) / 2;
    }
}
